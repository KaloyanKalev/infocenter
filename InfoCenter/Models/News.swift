//
//  News.swift
//  InfoCenter
//
//  Created by Kaloyan Kalev on 2/20/19.
//  Copyright © 2019 Kaloyan Kalev. All rights reserved.
//

import Foundation

struct ArticleList: Decodable {
    let articles: [Article]
}
struct Article: Decodable {
    let title: String
    let description: String
}


