import UIKit
import CoreLocation
import UserNotifications

class ViewController: UIViewController, CLLocationManagerDelegate {

    private let locationManager = CLLocationManager()
    let geoCoder = CLGeocoder()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupLocationManager()
        setup()
    }
    
    private func setupLocationManager() {
        locationManager.requestAlwaysAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let currentLocation = locations.first else {return}
        
        geoCoder.reverseGeocodeLocation(currentLocation) { (placemarks, error) in
            guard let currentLocPlacemark = placemarks?.first else { return }
            print(Double(currentLocPlacemark.location?.coordinate.latitude ?? 0.0))
            print(Double(currentLocPlacemark.location?.coordinate.longitude ?? 0.0))
            print(currentLocPlacemark.country ?? "No country found")
            print(currentLocPlacemark.locality ?? "No locality found")
            print(currentLocPlacemark.isoCountryCode ?? "No country code found")
        }
    }
    
    private func setup() {
        
    }
}

