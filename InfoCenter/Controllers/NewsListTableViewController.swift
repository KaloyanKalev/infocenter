import Foundation
import UIKit

class NewsListTableViewController: UITableViewController {
    private var articleListVM: ArticleViewModel!
    private var activityIndicatorView: UIActivityIndicatorView!
    
    override func loadView() {
        super.loadView()
        activityIndicatorView = UIActivityIndicatorView(style: .gray)
        tableView.backgroundView = activityIndicatorView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    private func setup() {
        let newsService = NewsService(apiKey: Constants.newsApiKey.rawValue, countryIso: "us")
        activityIndicatorView.startAnimating()
        newsService.getNews(completion: { (articles) in
            if let articles = articles {
                self.articleListVM = ArticleViewModel(articles)
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                    self.activityIndicatorView.stopAnimating()
                }
            }
        })
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return self.articleListVM == nil ? 0 : self.articleListVM.numberOfSections
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.articleListVM.NumberofRowsInSection(section)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ArticleTableViewCell", for: indexPath) as? ArticleTableViewCell else {
            fatalError("ArticleTableViewCell not found.")
        }
        
        let articleVM = self.articleListVM.articleAtIndex(indexPath.row)
        let url = URL(string: articleVM.articles[indexPath.row]["urlToImage"] as? String ?? "")
        DispatchQueue.global(qos: .userInitiated).async {
            DispatchQueue.main.async {
                let data = try? Data(contentsOf: url!)
                if let imageData = data  {
                    cell.articleImage.image = UIImage(data: imageData)
                }
            }
        }
        
        cell.articleTitle.text = articleVM.articles[indexPath.row]["title"] as? String ?? ""
        cell.articleDescription.text = articleVM.articles[indexPath.row]["description"] as? String ?? ""
        return cell
    }
}
