//
//  NewsService.swift
//  InfoCenter
//
//  Created by Kaloyan Kalev on 2/21/19.
//  Copyright © 2019 Kaloyan Kalev. All rights reserved.
//

import Foundation

class NewsService {
    
    private var newsUrl: URL?
    
    typealias newsDictionaryHandler = ([Dictionary<String, Any>]?) -> Void
    
    init(apiKey: String, countryIso: String) {
        self.newsUrl = URL(string: "https://newsapi.org/v2/top-headlines?country=\(countryIso.lowercased())&apiKey=\(apiKey)")
    }
    
    func getNews(completion: @escaping newsDictionaryHandler) {
        let webService = WebService(url: self.newsUrl!)
        webService.getJson { (jsonNews) in
            completion(jsonNews?["articles"] as? [Dictionary<String, Any>])
        }
    }
    
}
