//
//  ArticleViewModel.swift
//  InfoCenter
//
//  Created by Kaloyan Kalev on 2/20/19.
//  Copyright © 2019 Kaloyan Kalev. All rights reserved.
//

import Foundation

class ArticleViewModel {
    
    var articles: [Dictionary<String,Any>] = []
    var numberOfSections: Int = 1

    init(_ articles: [Dictionary<String,Any>]) {
        self.articles = articles
    }
    
    func NumberofRowsInSection(_ section: Int) -> Int {
        return self.articles.count
    }
    
    func articleAtIndex(_ index: Int) -> ArticleViewModel {
        return ArticleViewModel(self.articles)
    }
}
