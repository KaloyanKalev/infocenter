//
//  NewsViewModel.swift
//  InfoCenter
//
//  Created by Kaloyan Kalev on 2/20/19.
//  Copyright © 2019 Kaloyan Kalev. All rights reserved.
//

import Foundation

struct ArticleViewModel {
    private let article: Article
}

extension ArticleViewModel {
    init(_ article: Article) {
        self.article = article
    }
}
