//
//  ArticleTableViewCell.swift
//  InfoCenter
//
//  Created by Kaloyan Kalev on 2/24/19.
//  Copyright © 2019 Kaloyan Kalev. All rights reserved.
//

import Foundation
import UIKit

class ArticleTableViewCell: UITableViewCell {
    
    @IBOutlet weak var articleImage: UIImageView!
    @IBOutlet weak var articleTitle: UILabel!
    @IBOutlet weak var articleDescription: UILabel!
}
